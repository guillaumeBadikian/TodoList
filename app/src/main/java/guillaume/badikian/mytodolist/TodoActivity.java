package guillaume.badikian.mytodolist;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.ExtractedTextRequest;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by badikiag on 15/01/2018.
 */
public class TodoActivity extends Activity implements View.OnClickListener{

    private Button btnValider ;
    private Button btnClear;
    private TextView text;
    private EditText editText;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_layout);
        btnValider = (Button)findViewById(R.id.buttonvalidate);
        btnClear = (Button)findViewById(R.id.buttonclearlist);
        text = (TextView)findViewById(R.id.textView);
        editText = (EditText)findViewById(R.id.extractEditText);
        btnValider.setOnClickListener(this) ;
        btnClear.setOnClickListener(this);
        

    }


    @Override
    public void onClick(View v) {
        if(v==btnValider)
        {
            text.setText(editText.getText().toString());
        }
        if(v==btnClear)
        {
            text.setText("");
        }


    }
}












































..